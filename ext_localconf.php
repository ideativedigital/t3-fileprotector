<?php

defined('TYPO3_MODE') or die('Access denied.');

(static function($_EXTKEY) {
    $extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY);

    /** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $dispatcher */
    $dispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);

    // Connect to the PreGeneratePublicUrl signal for files' url generation
    $dispatcher->connect(
        \TYPO3\CMS\Core\Resource\ResourceStorage::class,
        \TYPO3\CMS\Core\Resource\ResourceStorage::SIGNAL_PreGeneratePublicUrl,
        \Ideative\IdFileprotector\Signal\PreGeneratePublicUrlSlot::class,
        'execute'
    );

    // Configure the plugin that will be used as public URL, requiring some extra information to download
    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
        "Ideative.$extensionName",
        'FileProtector',
        [
            'File' => 'requestDownloadForm,validateRequestForm,downloadLinkEmailConfirmation,downloadFile'
        ],
        [
            'File' => 'requestDownloadForm,validateRequestForm,downloadLinkEmailConfirmation,downloadFile'
        ]
    );

})('id_fileprotector');
