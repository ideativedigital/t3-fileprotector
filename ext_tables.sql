#
# Table structure for table 'sys_file_metadata'
#
CREATE TABLE sys_file_metadata (

	is_protected tinyint(4) unsigned DEFAULT '0' NOT NULL

);

#
# Table structure for table 'tx_idfileprotector_domain_model_downloadrequest'
#
CREATE TABLE tx_idfileprotector_domain_model_downloadrequest (

    uid int(11) NOT NULL auto_increment,
    pid int(11) DEFAULT '0' NOT NULL,

    uuid VARCHAR(255) DEFAULT '' NOT NULL,
	file int(11) NOT NULL,
	expiration_date int(11),
	data TEXT DEFAULT '' NOT NULL,


    tstamp int(11) unsigned DEFAULT '0' NOT NULL,
    crdate int(11) unsigned DEFAULT '0' NOT NULL,
    cruser_id int(11) unsigned DEFAULT '0' NOT NULL,
    deleted smallint(5) unsigned DEFAULT '0' NOT NULL,

    PRIMARY KEY (uid),
    KEY parent (pid),

);
