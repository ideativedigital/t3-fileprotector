var Encore = require('@symfony/webpack-encore')

if (!Encore.isRuntimeEnvironmentConfigured()) {
    Encore.configureRuntimeEnvironment(process.env.NODE_ENV || 'dev')
}

Encore
    /**
     * Change intranet_package to the actual package name
     */
    .setOutputPath('Resources/Public/Assets')
    .setPublicPath('/typo3conf/ext/id_fileprotector/Resources/Public/Assets')
    .setManifestKeyPrefix('build/')

    /**
     * Here is where you add some specific page entries, which you need to load in TS:
     *
     * page.includeJsFooter.someExtensionKey = typo3_encore:entryName
     * page.includeCSS.someExtensionKey = typo3_encore:entryName
     *
     * Everything else is loaded through the js entrypoint, like
     * import somePlugin from 'some-plugin'
     *
     * and same for CSS
     * import 'some-plugin/dist/css/main'
     *
     */
    .addEntry('backend', './Resources/Private/Assets/JavaScript/backend/')

    .cleanupOutputBeforeBuild()
    .enableSingleRuntimeChunk()
    .enableSourceMaps(!Encore.isProduction())
    .enableVersioning(Encore.isProduction())

    .enableSassLoader()
    .enableEslintLoader(eslintConfig => (eslintConfig.formatter = 'codeframe'))
    .configureBabel(babelConfig => {
        babelConfig.plugins.push('@babel/plugin-proposal-class-properties')
    }, { useBuiltIns: 'usage', corejs: 3 })
    .enableVueLoader(() => {}, { runtimeCompilerBuild: false })

const config = Encore.getWebpackConfig()
config.watchOptions = {
    poll: true
}

module.exports = config
