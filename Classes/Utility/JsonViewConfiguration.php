<?php

namespace Ideative\IdFileprotector\Utility;

/**
 * Class JsonViewConfiguration: A utility class to create the JsonView configurations
 * @package Ideative\IdFileprotector\Utility
 */
class JsonViewConfiguration
{
    public const ALL = '_descendAll';
    public const EXCLUDE = '_exclude';
    public const ONLY = '_only';
    public const PROPERTIES = '_descend';

    /**
     * Default configuration for lists of download requests
     */
    public const REQUEST_LIST_CONFIGURATION = [
        self::ALL => [
            self::ONLY => [ 'uuid', 'expirationDate', 'data', 'crdate', 'uid', 'pid', 'file' ],
            self::PROPERTIES => [
                'expirationDate' => [],
                'data' => [],
                'crdate' => [],
                'file' => [
                    self::EXCLUDE => [
                        'combinedIdentifier', 'contents', 'creationTime', 'deleted', 'forLocalProcessing', 'indexed', 'missing', 'type'
                    ]
                ]
            ]
        ]
    ];
}
