<?php

namespace Ideative\IdFileprotector\Utility;

use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Configuration\Exception\InvalidConfigurationTypeException;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class Configuration implements SingletonInterface
{
    /**
     * The name of the extension
     * @var string
     */
    public const EXTENSION_NAME = 'id_fileprotector';

    /**
     * Name of the signal slot to validate the form download request
     * @var string
     */
    public const SIGNAL_ValidateDownloadRequestForm = 'preValidateDownloadRequestForm';


    /**
     * Should the direct download link be sent by email instead of directly downloaded
     * @return bool
     */
    public static function shouldSendLinkByEmail(): bool
    {
        return self::getExtensionConfiguration()['sendLinkByEmail'] ?? false;
    }

    /**
     * Retrieve the typenum for the download form plugin popup pagetype
     * @return int
     */
    public static function getFormPageType(): int
    {
        return (int)self::getPluginTyposcriptConfiguration()['config.']['formPageTypeNum'];
    }

    /**
     * Get the configured page id for the download request form
     * @return int|null
     */
    public static function getFormPageUid(): ?int
    {
        $value = self::getExtensionConfiguration()['formPageUid'];
        return !empty($value) ? (int)$value : null;
    }

    /**
     * Get the configured list of rootlines to enable the url override in
     * @return array
     */
    public static function getRootLines(): array
    {
        return explode(',', self::getExtensionConfiguration()['rootLine'] ?? '');
    }

    /**
     * Get the configured page id for the download request form
     * @return string
     */
    public static function getLinkEmailSubject(): string
    {
        return self::getExtensionConfiguration()['linkEmailSubject'];
    }

    /**
     * Get the configured page id for the download request form
     * @return string
     */
    public static function getLinkExpirationDuration(): string
    {
        return self::getExtensionConfiguration()['linkExpiracy'];
    }

    /**
     * Retrieve the whole extension configuration as an array.
     *
     * @return array
     */
    public static function getAllConfiguration(): array
    {
        return array_merge(
            [],
            self::getExtensionConfiguration(),
            self::getFrameworkConfiguration(),
            self::getPluginTyposcriptConfiguration()
        );
    }

    /**
     * Retrieve ext_conf_template.txt configuration values as an array
     *
     * @return array
     */
    public static function getExtensionConfiguration(): array
    {
        if (version_compare(TYPO3_version, '10', '>')) {
            $conf = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get(self::EXTENSION_NAME);
        } else {
            $conf = unserialize($GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf'][ self::EXTENSION_NAME ], [
                'allowed_classes' => false
            ]);
        }

        return $conf ?? [];
    }

    /**
     * Get extbase framework configuration as an array
     *
     * @return array
     */
    public static function getFrameworkConfiguration(): array
    {
        if (version_compare(TYPO3_version, '10', '>')) {
            $configurationManager = GeneralUtility::makeInstance(ConfigurationManagerInterface::class);
        } else {
            $configurationManager = GeneralUtility::makeInstance(ObjectManager::class)->get(ConfigurationManager::class);
        }

        try {
            $frameworkConfig = $configurationManager
                ->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK, self::EXTENSION_NAME);
        } catch (InvalidConfigurationTypeException $e) {
            return [];
        }

        return $frameworkConfig ?? [];
    }

    /**
     * Get the typoscript configuration for the plugins of this extension as an array
     * @return array
     */
    public static function getPluginTyposcriptConfiguration(): array
    {
        $ext = str_replace('_', '', self::EXTENSION_NAME);
        return $GLOBALS['TSFE']->tmpl->setup['plugin.']["tx_$ext."] ?? [];
    }
}
