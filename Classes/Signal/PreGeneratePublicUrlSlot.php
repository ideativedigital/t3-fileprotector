<?php

namespace Ideative\IdFileprotector\Signal;

use Ideative\IdFileprotector\Utility\Configuration;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Web\Routing\UriBuilder;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Class PreGeneratePublicUrlSlot: Signal slot that overrides the public URL of protected files
 * @package Ideative\IdFileprotector\Signal
 */
class PreGeneratePublicUrlSlot implements SingletonInterface
{

    /**
     * Execute the signal slot.
     *
     * Called with a reference to the $fileData, allowing to alter its 'publicUrl' field if the
     * file is protected and set it to the wanted form !
     *
     * @param $caller
     * @param $driver
     * @param File $file
     * @param $relativeToCurrentScript
     * @param $fileData
     */
    public function execute($caller, $driver, $file, $relativeToCurrentScript, $fileData): void
    {
        $tsfe = $GLOBALS['TSFE'];

        // Loop through each parent page and see if it matches any of the configured rootlines
        $isPidInRootLines = TYPO3_MODE === 'FE' && array_reduce($tsfe->rootLine, static function ($isInLine, $page) {
            return $isInLine || in_array($page['uid'], Configuration::getRootLines(), false);
        });

        // Exit early if the page is not in the configured rootlines or the file is not protected
        if (!$isPidInRootLines || !($file instanceof File) || !($file->getProperty('is_protected') ?? false))  {
            return;
        }

        if (version_compare(TYPO3_version, '10', '>')) {
            $uriBuilder = GeneralUtility::makeInstance(UriBuilder::class);
        } else {
            $uriBuilder = GeneralUtility::makeInstance(ObjectManager::class)->get(UriBuilder::class);
        }

        // Prepare URI builder with common parameters
        $uriBuilder->reset();

        $formPageUid = Configuration::getFormPageUid();
        if ($formPageUid) {
            // If a page uid is configured for the form, use it to build the Uri
            $uriBuilder->setTargetPageUid($formPageUid);
        } else {
            // Else use current page with custom page type (to use in lightbox)
            $uriBuilder
                ->setTargetPageUid($tsfe->id)
                ->setTargetPageType(Configuration::getFormPageType());
        }

        // Build the url
        $url = $uriBuilder->uriFor(
            'requestDownloadForm',
            [ 'file' => $file->getUid() ],
            'File',
            'IdFileprotector',
            'FileProtector'
        );

        // Replace it
        $fileData['publicUrl'] = $url;
    }

}
