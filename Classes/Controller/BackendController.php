<?php

namespace Ideative\IdFileprotector\Controller;

use Ideative\IdFileprotector\Domain\Model\DownloadRequest;
use Ideative\IdFileprotector\Domain\Repository\DownloadRequestRepository;
use Ideative\IdFileprotector\Domain\Repository\FileRepository;
use Ideative\IdFileprotector\Utility\JsonViewConfiguration;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\View\JsonView;

/**
 * Class BackendController : Controller used for the backend module as an API.
 * @package Ideative\IdFileprotector\Controller
 */
class BackendController extends ActionController
{
    /** @var string Namespace of the backend module */
    public const MODULE_NAMESPACE = 'tx_idfileprotector_file_idfileprotectorfileprotector';

    /** @var JsonView */
    protected $view;

    /** @var string */
    protected $defaultViewObjectName = JsonView::class;

    /** @var DownloadRequestRepository */
    protected $downloadRequestRepository;

    /** @var FileRepository */
    protected $fileRepository;


    /**
     * List all the protected files with optional pagination
     *
     * @param int $page
     */
    public function filesAction(int $page = 0): void
    {
        /** @var DownloadRequest[] $requests */
        $protectedFiles = $this->fileRepository->findAllProtected($page);
        $total = $this->fileRepository->countAllProtected();

        header("X-Total-Rows: $total");
        $this->view->assign('value', $protectedFiles);

        $this->view->setConfiguration([ 'value' => [
            JsonViewConfiguration::ALL => [
                JsonViewConfiguration::EXCLUDE => [
                    'combinedIdentifier', 'contents', 'creationTime', 'deleted', 'forLocalProcessing', 'indexed', 'missing', 'type'
                ]
            ]
        ]]);
    }

    /**
     * List the requests with optional pagination
     * @param int $page
     */
    public function requestsAction(int $page = 0): void
    {
        $requests = $this->downloadRequestRepository->findAllPaginated($page);
        $total = $this->downloadRequestRepository->countAll();

        header("X-Total-Rows: $total");
        $this->view->assign('value', $requests);

        $this->view->setConfiguration([ 'value' => JsonViewConfiguration::REQUEST_LIST_CONFIGURATION ]);
    }

    /**
     * List distinct emails that requested some files with optional pagination
     *
     * @param int $page
     */
    public function usersAction(int $page = 0): void
    {
        $requests = $this->downloadRequestRepository->findDistinctEmails($page);
        $total = $this->downloadRequestRepository->countDistinctEmails();

        header("X-Total-Rows: $total");
        $this->view->assign('value', $requests);
    }

    /**
     * List the download requests for the given file id
     * @param int $file
     */
    public function fileDetailAction(int $file): void
    {
        $requests = $this->downloadRequestRepository->findByFile($file);
        $this->view->assign('value', $requests);

        $viewConfig = JsonViewConfiguration::REQUEST_LIST_CONFIGURATION;
        unset($viewConfig[JsonViewConfiguration::ALL][JsonViewConfiguration::PROPERTIES]['file']);

        $this->view->setConfiguration([ 'value' => $viewConfig ]);
    }

    /**
     * List the download requests for the given email
     * @param string $email
     */
    public function userDetailAction(string $email): void
    {
        $requests = $this->downloadRequestRepository->findByEmail($email);
        $this->view->assign('value', $requests);

        $this->view->setConfiguration([ 'value' => JsonViewConfiguration::REQUEST_LIST_CONFIGURATION ]);
    }


    //<editor-fold desc="Dependency injection" defaultstate="collasped">
    public function injectDownloadRequestRepository(DownloadRequestRepository $downloadRequestRepository): void
    {
        $this->downloadRequestRepository = $downloadRequestRepository;
    }
    public function injectFileRepository(FileRepository $fileRepository): void
    {
        $this->fileRepository = $fileRepository;
    }
    //</editor-fold>
}
