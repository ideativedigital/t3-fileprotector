<?php

namespace Ideative\IdFileprotector\Controller;


use Exception;
use http\Exception\InvalidArgumentException;
use Ideative\IdFileprotector\Domain\Model\DownloadRequest;
use Ideative\IdFileprotector\Domain\Repository\DownloadRequestRepository;
use Ideative\IdFileprotector\Utility\Configuration;
use Symfony\Component\Mime\Address;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\MailUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Mvc\Exception\StopActionException;
use TYPO3\CMS\Extbase\Mvc\Exception\UnsupportedRequestTypeException;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotException;
use TYPO3\CMS\Extbase\SignalSlot\Exception\InvalidSlotReturnException;
use TYPO3\CMS\Fluid\View\StandaloneView;

class FileController extends ActionController
{

    /** @var FileRepository */
    protected $fileRepository;

    /** @var DownloadRequestRepository */
    protected $downloadRequestRepository;


    /**
     * Shows the request form for given file
     *
     * @param string $file
     * @throws StopActionException
     */
    public function requestDownloadFormAction(string $file): void
    {
        /** @var File $meta */
        $meta = $this->fileRepository->findByUid($file);

        // If for some reason, the user arrived here for an unprotected file, download it right away
        if ((bool)$meta->getProperty('is_protected') !== true) {
            $this->forward('downloadFile', null, null, [
                'file' => $meta->getUid(),
                'force' => true
            ]);
        }

        // Else show them the request form
        $this->view->assign('file', $meta);
    }

    /**
     * Validate the form (via a Signal slot, optionally) and either redirect the user to the file or send them a link
     * by email
     *
     * @param string $file
     * @throws IllegalObjectTypeException
     * @throws InvalidSlotException
     * @throws InvalidSlotReturnException
     * @throws StopActionException
     * @throws UnsupportedRequestTypeException
     * @throws NoSuchArgumentException
     */
    public function validateRequestFormAction(string $file): void
    {
        /** @var File $meta */
        $meta = $this->fileRepository->findByUid($file);
        $form = $this->request->getArgument('form');

        // Perform the validation on the signal slot, throwing an exception if invalid, otherwise, the request is considered valid
        $this->signalSlotDispatcher->dispatch(
            __CLASS__,
            Configuration::SIGNAL_ValidateDownloadRequestForm,
            [ 'form' => &$form ]
        );

        $url = $this->createDownloadRequestAndGetUrl($meta, $form);

        if (Configuration::shouldSendLinkByEmail()) {
            if (empty($form['email'])) {
                throw new InvalidArgumentException("No email was found ! Please check your validation signal slot.");
            }

            // Send $url by email
            $this->sendUrlByEmail($url, $form);

            $url = $this->uriBuilder
                ->reset()
                ->setAddQueryString(true)
                ->uriFor('downloadLinkEmailConfirmation', [ 'email' => $form['email'] ]);

            // Confirm to the user that they received the link by email
            $this->redirectToUri($url);
        } else {
            $this->redirectToUri($url);
        }
    }

    /**
     * Display the confirmation that the link was sent by email
     * @param string $email
     */
    public function downloadLinkEmailConfirmationAction(string $email): void
    {
        $this->view->assign('email', $email);
    }

    /**
     * Send the file to the user's browser, after verifying they can see it
     *
     * @param string $file
     * @param string $uuid
     * @param bool $force
     */
    public function downloadFileAction(string $file, string $uuid = '', bool $force = false): void
    {
        /** @var File $meta */
        $meta = $this->fileRepository->findByUid($file);

        $isFileProtected = $meta->getProperty('is_protected') ?? false;
        $bypassVerification = $force || !$isFileProtected;

        if (!$bypassVerification && (empty($uuid) || !$this->downloadRequestRepository->isRequestValidByUuidAndFile($uuid, $meta))) {
            http_response_code(403);
            die("Forbidden");
        }

        $meta->getStorage()->dumpFileContents($meta);
        exit;
    }

    /**
     * Action to show the backend module. Passes a URL 'template' that is used to make REST calls
     */
    public function backendShowDownloadRequestsAction(): void
    {
        $paginationUrl = $this->uriBuilder
            ->reset()
            ->setCreateAbsoluteUri(true)
            ->setAddQueryString(true)
            ->setArguments([ BackendController::MODULE_NAMESPACE => [
                'controller' => 'Backend',
                'action' => '##action##',
                'page' => '##page##'
            ]])
            ->buildBackendUri();

        $this->view->assign('apiUrl', $paginationUrl);
    }

    /**
     * Create a persisted download request and return its download url
     *
     * @param File $file
     * @param $data
     * @return string
     * @throws IllegalObjectTypeException
     * @throws Exception
     */
    private function createDownloadRequestAndGetUrl(File $file, $data): string
    {
        $downloadRequest = DownloadRequest::new($file, $data);
        $this->downloadRequestRepository->add($downloadRequest);

        return $this->uriBuilder
            ->reset()
            ->setAddQueryString(true)
            ->setCreateAbsoluteUri(true)
            ->setTargetPageUid($GLOBALS['TSFE']->id)
            ->uriFor('downloadFile', [ 'file' => $file->getUid(), 'uuid' => $downloadRequest->getUuid() ]);
    }

    /**
     * Send the given link to the given address by email
     *
     * @param string $url
     * @param array $form
     */
    private function sendUrlByEmail(string $url, array $form): void
    {
        /** @var StandaloneView $emailView */
        $emailView = $this->objectManager->get(StandaloneView::class);

        $tsViewConfiguration = Configuration::getPluginTyposcriptConfiguration()['view.'];
        $emailView->setTemplateRootPaths($tsViewConfiguration['templateRootPaths.']);
        $emailView->setLayoutRootPaths($tsViewConfiguration['layoutRootPaths.']);
        $emailView->setPartialRootPaths($tsViewConfiguration['layoutRootPaths.']);

        $emailView->setTemplate('Email/Link.html');
        $emailView->assign('url', $url);
        $emailView->assign('form', $form);

        $to = $form['email'];
        $from = MailUtility::getSystemFrom();
        $returnPath = MailUtility::getSystemFromAddress();
        $subject = Configuration::getLinkEmailSubject();
        $html = $emailView->render();

        if (version_compare(TYPO3_version, '10', '>')) {
            GeneralUtility::makeInstance(MailMessage::class)
                ->subject($subject)
                ->from($from)
                ->to(new Address($to))
                ->html($html)
                ->setReturnPath($returnPath)
                ->send();
        } else {
            GeneralUtility::makeInstance(MailMessage::class)
                ->setSubject($subject)
                ->setFrom($from)
                ->setTo([ $to ])
                ->setBody($html, 'text/html')
                ->setReturnPath($returnPath)
                ->send();
        }
    }


    //<editor-fold desc="Dependency injection" defaultstate="collasped">
    public function injectFileRepository(FileRepository $fileRepository): void
    {
        $this->fileRepository = $fileRepository;
    }
    public function injectDownloadRequestRepository(DownloadRequestRepository $downloadRequestRepository): void
    {
        $this->downloadRequestRepository = $downloadRequestRepository;
    }
    //</editor-fold>

}
