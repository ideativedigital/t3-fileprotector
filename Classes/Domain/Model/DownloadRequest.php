<?php

namespace Ideative\IdFileprotector\Domain\Model;


use DateInterval;
use DateTime;
use Exception;
use Ideative\IdFileprotector\Utility\Configuration;
use JsonSerializable;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\ProcessedFileRepository;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;
use TYPO3\CMS\Extbase\Object\ObjectManager;

class DownloadRequest extends AbstractEntity
{

    /**
     * A unique id (used in the download link)
     *
     * @var string
     */
    protected $uuid;

    /**
     * The file for which the download request is
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     */
    protected $file;

    /**
     * The expiration date of the link
     * @var \DateTime
     */
    protected $expirationDate;

    /**
     * The request data (containing all fields from the form, if any)
     * @var string
     */
    protected $data;

    /**
     * The create date
     * @var \DateTime
     */
    protected $crdate;


    /**
     * Create a new instance with the given data
     *
     * @param File $file
     * @param array $data
     * @return static
     * @throws Exception
     */
    public static function new(File $file, $data = []): self
    {
        return GeneralUtility::makeInstance(self::class)
            ->setUuid(uniqid('', true))
            ->setFile($file)
            ->setExpirationDate((new DateTime())->add(new DateInterval(Configuration::getLinkExpirationDuration())))
            ->setData($data);
    }

    /**
     * @return string
     */
    public function getUuid(): string
    {
        return $this->uuid;
    }

    /**
     * @param string $uuid
     * @return DownloadRequest
     */
    public function setUuid(string $uuid): self
    {
        $this->uuid = $uuid;
        return $this;
    }

    /**
     * @return File
     */
    public function getFile(): File
    {
        return $this->file->getOriginalResource()->getOriginalFile();
    }

    /**
     * @param File $file
     * @return DownloadRequest
     */
    public function setFile(File $file): self
    {
        $falReference = ResourceFactory::getInstance()->createFileReferenceObject([
            'uid_local' => $file->getUid(),
            'uid_foreign' => uniqid('NEW_'),
            'uid' => uniqid('NEW_'),
        ]);

        if (version_compare(TYPO3_version, '10', '>')) {
            $reference = GeneralUtility::makeInstance(FileReference::class);
        } else {
            $reference = GeneralUtility::makeInstance(ObjectManager::class)->get(FileReference::class);
        }

        $reference->setOriginalResource($falReference);

        $this->file = $reference;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getExpirationDate()
    {
        return $this->expirationDate;
    }

    /**
     * @param mixed $expirationDate
     * @return DownloadRequest
     */
    public function setExpirationDate($expirationDate): self
    {
        $this->expirationDate = $expirationDate;
        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return json_decode($this->data, true);
    }

    /**
     * @param mixed $data
     * @return DownloadRequest
     */
    public function setData($data): self
    {
        $this->data = json_encode($data);
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCrdate(): ?DateTime
    {
        return $this->crdate;
    }

    /**
     * @param DateTime $crdate
     * @return DownloadRequest
     */
    public function setCrdate(DateTime $crdate): self
    {
        $this->crdate = $crdate;
        return $this;
    }

}
