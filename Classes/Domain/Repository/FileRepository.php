<?php

namespace Ideative\IdFileprotector\Domain\Repository;

use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Resource\FileRepository as BaseFileRepository;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class FileRepository : Custom extend of the Core file repository to add custom needed methods
 * @package Ideative\IdFileprotector\Domain\Repository
 */
class FileRepository extends BaseFileRepository
{
    /** @var string The name of the default table this repository handles */
    private $tableName = 'sys_file';

    /**
     * Find all files that are protected with optional pagination. If $page is 0, then no pagination is performed
     *
     * @param int $page Page number, starts at one
     * @param int $perPage Number of elements per page, if not using default 20
     * @return File[]
     */
    public function findAllProtected($page = 0, $perPage = 20): array
    {
        $q = $this->getBaseProtectedFilesQuery();

        if ($page > 0) {
            $q->setMaxResults($perPage)->setFirstResult(($page - 1) * $perPage);
        }

        $rows = $q->select('f.*')->execute()->fetchAll();

        $files = [];
        foreach ($rows as $row) {
            $files[] = $this->createDomainObject($row);
        }

        return $files;
    }

    /**
     * Count the number of protected files. Useful for the REST total header.
     *
     * @return int
     */
    public function countAllProtected(): int
    {
        return $this->getBaseProtectedFilesQuery()->count('*')->execute()->rowCount();
    }

    /**
     * Returns the base query for protected files. Can be used in select or count.     *
     * @return QueryBuilder
     */
    protected function getBaseProtectedFilesQuery(): QueryBuilder
    {
        $q = $this->getQueryBuilder();

        return $q
            ->from($this->tableName, 'f')
            ->innerJoin('f', 'sys_file_metadata', 'm',
                $q->expr()->eq('m.file', $q->quoteIdentifier('f.uid'))
            )
            ->where('m.is_protected', 1);
    }

    /**
     * Get a QueryBuilder instance
     * @return QueryBuilder
     */
    public function getQueryBuilder(): QueryBuilder
    {
        return GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($this->tableName);
    }
}
