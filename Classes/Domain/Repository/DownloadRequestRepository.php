<?php

namespace Ideative\IdFileprotector\Domain\Repository;

use DateTime;
use Ideative\IdFileprotector\Domain\Model\DownloadRequest;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;
use TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper;
use TYPO3\CMS\Extbase\Persistence\QueryResultInterface;
use TYPO3\CMS\Extbase\Persistence\Repository;

/**
 * Class DownloadRequestRepository : Repository that handles the download requests
 * @package Ideative\IdFileprotector\Domain\Repository
 */
class DownloadRequestRepository extends Repository
{
    /** @var string The name of this repo's table */
    public $tableName = 'tx_idfileprotector_domain_model_downloadrequest';

    /** @var DataMapper */
    protected $dataMapper;


    /**
     * Find all the requests with optional pagination.
     *
     * @param int $page The page number, starting at 1. If 0, no pagination is made.
     * @param int $perPage The number of elements per page, if not the default 20
     * @return array|QueryResultInterface
     */
    public function findAllPaginated($page = 0, $perPage = 20)
    {
        $q = $this->createQuery();
        $q->getQuerySettings()->setRespectStoragePage(false);

        if ($page > 0) {
            $q->setLimit($perPage)->setOffset(($page - 1) * $perPage);
        }

        return $q->execute();
    }

    /**
     * Count all the download requests
     *
     * @return int
     */
    public function countAll(): int
    {
        $q = $this->createQuery();
        $q->getQuerySettings()->setRespectStoragePage(false);
        return $q->execute()->count();
    }

    /**
     * Checks if the request matching the given uuid and file exists and is still valid
     *
     * @param string $uuid
     * @param File $file
     * @return bool
     */
    public function isRequestValidByUuidAndFile(string $uuid, File $file): bool
    {
        $q = $this->createQuery();
        $q->matching($q->logicalAnd([
            $q->equals('file.uid_local', $file->getUid()),
            $q->equals('uuid', $uuid)
        ]));

        /** @var DownloadRequest $request */
        $request = $q->execute()->getFirst();

        if ($request === null) {
            return false;
        }

        return $request->getExpirationDate() > (new DateTime());
    }

    /**
     * Find all download requests for a given file
     *
     * @param int $fileUid
     * @return QueryResultInterface
     */
    public function findByFile(int $fileUid): QueryResultInterface
    {
        $q = $this->createQuery();
        $q->getQuerySettings()->setRespectStoragePage(false);

        return $q
            ->matching($q->logicalAnd([
                $q->equals('file.uid_local', $fileUid)
            ]))
            ->execute();
    }

    /**
     * Find all distinct emails that made a download request, with optional pagination.
     *
     * @param int $page The page number, starting at 1. If 0, no pagination is made.
     * @param int $perPage The number of elements per page, if not the default 20
     * @return array
     */
    public function findDistinctEmails(int $page = 0, $perPage = 20): array
    {
        $q = $this->getQueryBuilder();

        $query = $q
            ->selectLiteral(
                "JSON_UNQUOTE(JSON_EXTRACT(r.data, '$.email')) AS email",
                "COUNT(*) as requests"
            )
            ->from($this->tableName, 'r')
            ->groupBy('email');

        if ($page > 0) {
            $q->setMaxResults($perPage)->setFirstResult(($page - 1) * $perPage);
        }

        if (version_compare(TYPO3_version, '10', '>')) {
            return $query->execute()->fetchAllAssociative();
        } else {
            return $query->execute()->fetchAll();
        }
    }

    /**
     * Returns the total count of distinct emails that made a download request.
     * Useful for REST total
     *
     * @return int
     */
    public function countDistinctEmails(): int
    {
        $q = $this->getQueryBuilder();

        return $q
            ->selectLiteral("COUNT(DISTINCT JSON_EXTRACT(r.data, '$.email'))")
            ->from($this->tableName, 'r')
            ->execute()
            ->fetchColumn(0);
    }

    /**
     * Returns a list of all requests made by a given email.
     *
     * @param string $email
     * @return array
     */
    public function findByEmail(string $email): array
    {
        $q = $this->getQueryBuilder();

        $query = $q
            ->select('*')
            ->from($this->tableName, 'r')
            ->where(
                "JSON_UNQUOTE(JSON_EXTRACT(r.data, '$.email')) = " . $q->createNamedParameter($email)
            );

        if (version_compare(TYPO3_version, '10', '>')) {
            $records = $query->execute()->fetchAllAssociative();
        } else {
            $records = $query->execute()->fetchAll();
        }

        return $this->dataMapper->map(DownloadRequest::class, $records);
    }

    /**
     * Override the default repository add methods, adding a persist right after adding the model.
     * @param object $object
     * @throws IllegalObjectTypeException
     */
    public function add($object)
    {
        parent::add($object);
        $this->persistenceManager->persistAll();
    }


    /**
     * Return a QueryBuilder instance for this repository
     * @return QueryBuilder
     */
    public function getQueryBuilder(): QueryBuilder
    {
        return GeneralUtility::makeInstance(ConnectionPool::class)
            ->getQueryBuilderForTable($this->tableName);
    }

    //<editor-fold desc="Dependency injection" defaultstate="collasped">
    public function injectDataMapper(DataMapper $dataMapper): void
    {
        $this->dataMapper = $dataMapper;
    }
    //</editor-fold>

}
