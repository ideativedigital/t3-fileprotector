![](./Documentation/Images/IdeativeLogo.png)

# TYPO3 Extension FileProtector

This extension provides a simple way to protect files from direct download and require a form submission beforehands.

[![Monthly Downloads](https://poser.pugx.org/ideative/t3-fileprotector/d/monthly?format=flat)](https://packagist.org/packages/ideativedigital/t3-fileprotector)
[![License](https://poser.pugx.org/ideative/t3-fileprotector/license?format=flat)](https://packagist.org/packages/ideativedigital/t3-fileprotector)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)


## 1. How it works
The extension uses a Signal Slot in the public URL generation process for files. So everytime a file needs its public URL,
the extension will first check if it is protected (via an additional TCEForm database field), and if so, replace the said URL 
with a link to a custom plugin. This plugin will then show a form to the user, and when the form is submitted and validated,
 generate an actual unique and temporary link to the said file. 

## 2. Usage

### 1) Installation

#### Installation using Composer

The recommended way to install the extension is by using [Composer][1]. In your Composer based TYPO3 project root, just execute the following command 

```shell
composer require ideative/t3-fileprotector
``` 

#### Installation as extension from TYPO3 Extension Repository (TER)

Download and install the extension with the extension manager module.



### 2) Minimal setup

Out of the box, the extension works and is usable. With that said, you should and will probably wait to customize its
behavior and/or styling. 

#### Extension configuration
To do so, you can start by modifying the available extension settings :
![](./Documentation/Images/Basic_settings.png)

![](./Documentation/Images/Advanced_settings.png) 


#### TypoScript / Fluid templates
Another thing you can do is to override some of the extension's TypoScript and update, for example the default `storagePid`
to define where to store whe download requests, change the Fluid root paths to override templates, or include some CSS in the custom pagetype, like so :

```typo3_typoscript
plugin.tx_idfileprotector {
    # Always 42...
    persistence.storagePid = 42 

    view {
        templateRootPaths.10 = EXT:your_site_package/Resources/Private/Templates/FileProtector/
        partialRootPaths.10 = EXT:your_site_package/Resources/Private/Partials/FileProtector/
        layoutRootPaths.10 = EXT:your_site_package/Resources/Private/Layouts/FileProtector/
    }
}

module.tx_idfileprotector_file_idfileprotectorfileprotector.persistence.storagePid < plugin.tx_idfileprotector.persistence.storagePid

fileProtectorPopup.includeCSS.theme = EXT:your_site_package/Resources/Public/Css/theme.css
```


### 3) Editors usage

#### Enabling protection
In the filelist module, when editing a file, you now have a new 'Protection' tab. This allows you to check a box, requiring
the form completion for the said file.

![](./Documentation/Images/TCEFORM_protection.png)

#### Seeing requests
In the backend, under the main "File" module, there is now a new "File protection" module. 

![](./Documentation/Images/Backend_module_entry.png)

This module allows editors to 
see which files have been downloaded, when and by whom, plus all the data they filled in the download request form. 
Within the module itself, there is a subnavigation, allowing you to see the requests from 3 different perspectives :

- A list of all protected files (and then, for each file the requests)
- A list of all the download requests
- A list of the different emails that made download requests (and see all the requests for each email) 

## 3. Contributing

### 3.1. Pull requests

**Pull requests** are welcome! Nevertheless please be aware we might not read **PR**s very often and that we use this extension in a lot of projects, so we might not be able to adapt them all for a new feature. 

If you still want to submit a **PR**, don't forget to add an issue and connect it to your pull requests. This is very helpful to understand what kind of issue the **PR** is going to solve.

- Bugfixes: Please describe what kind of bug your fix solve and give us feedback how to reproduce the issue. We're going to accept only bugfixes if we can reproduce the issue.
- Features: Not every feature is relevant for us. In addition: We don't want to make the extension more complicated for an edge case feature. 

[1]: https://getcomposer.org/
