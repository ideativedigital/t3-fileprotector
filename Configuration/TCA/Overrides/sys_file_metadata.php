<?php

$extensionName = 'id_fileprotector';

$meta =& $GLOBALS['TCA']['sys_file_metadata'];

$meta['columns'] += [
    'is_protected' => [
        'exclude' => false,
        'label'=> "LLL:EXT:$extensionName/Resources/Private/Language/locallang_db.xlf:sys_file_metadata.is_protected",
        'config' => [
            'type' => 'check'
        ]
    ]
];

$meta['palettes'] += [
    'tx_idfileprotector_protection' => [
        'showitem' => 'is_protected,--linebreak--,download_requests'
    ]
];

$paletteString = '--div--;Protection,--palette--;Protection;tx_idfileprotector_protection';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_file_metadata', $paletteString);
