<?php

return [
    'ctrl' => [
        'title' => 'File download request',
        'label' => 'uuid',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'delete' => 'deleted',
        'enablecolumns' => [
        ],
        'searchFields' => 'data'
    ],
    'interface' => [
        'showRecordFieldList' => 'uuid,data',
    ],
    'types' => [
        '1' => [ 'showitem' => 'file,uuid,data' ],
    ],
    'columns' => [
        'file' => [
            'exclude' => true,
            'label' => 'File',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig('file', [
                'appearance' => [
                    'createNewRelationLinkTitle' => 'LLL:EXT:cms/locallang_ttc.xlf:images.addFileReference'
                ],

                'maxitems' => 1,

                'foreign_match_fields' => [
                    'fieldname' => 'file',
                    'tablenames' => 'tx_idfileprotector_domain_model_downloadrequest',
                    'table_local' => 'sys_file',
                ],
            ])
        ],
        'data' => [
            'exclude' => true,
            'label' => 'DATA',
            'config' => [
                'type' => 'text',
                'size' => 30,
                'eval' => 'trim,required',
                'max' => 76,
            ]
        ],
        'uuid' => [
            'exclude' => true,
            'label' => 'UUID',
            'config' => [
                'type' => 'input',
                'readOnly' => true,
                'eval' => '',
            ]
        ],
        'expiration_date' => [
            'exclude' => 1,
            'config' => [
                'type' => 'passthrough',
            ]
        ],
        'crdate' => [
            'exclude' => 1,
            'config' => [
                'type' => 'passthrough',
            ]
        ]
    ],
];
