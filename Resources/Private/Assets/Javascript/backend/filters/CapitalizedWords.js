/**
 * Returns the given string with its first letter uppercased
 * @param word
 * @returns {string}
 */
function capitalize (word) {
    return word.charAt(0).toUpperCase() + word.substring(1)
}

/**
 * Turns a camelCase, PascalCase or snake-case into a 'Human Case' string
 * @param string
 * @returns {string}
 */
export default function toCapitalizedWords (string) {
    const words = (string.match(/[A-Za-z][a-z]*/g) || [])
    return words.map(capitalize).join(' ')
}
