import day from 'dayjs'

/**
 * Returns the given date string as a readable date
 * @param date
 * @returns {string}
 */
export default function readableDate (date) {
    return day(date).format('DD/MM/YYYY HH:mm')
}
