import Vue from 'vue'
import App from './App'
const { BootstrapVue } = require('bootstrap-vue')

Vue.use(BootstrapVue)

// Global mixin to display error messages
Vue.mixin({
    methods: {
        /**
         * Show an error notification
         *
         * @param {String} message The text content of the notification
         * @param {Error} error The error object associated with the notification
         * @param {String} [title] The notification title
         */
        showError (message, error = null, title = 'An error occured') {
            message += '\nPlease check the developer console for more info.'

            try {
                // Use modern (9+) TYPO3 backend module
                window.require(['TYPO3/CMS/Backend/Notification'], function (Notification) {
                    Notification.error(title, message)
                })
            } catch (e) {
                // Fallback to old JS
                top.TYPO3.Notification.error(title, message)
            }

            // eslint-disable-next-line no-console
            console.error(message, error)
        }
    }
})

function waitFor (conditionFunction) {
    const poll = resolve => {
        if (conditionFunction()) resolve()
        else setTimeout(() => poll(resolve), 100)
    }

    return new Promise(poll)
}

(async function () {
    await waitFor(() => document.querySelector('#vue') != null)

    const el = document.querySelector('#vue')

    // Get the data-props
    const props = el.dataset

    // Set a global api url
    Vue.prototype.$apiUrl = props.apiUrl

    window.vueApp = new Vue({
        el,
        render: h => h(App, { props }),
        components: { App }
    })
})()
