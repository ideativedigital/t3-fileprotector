import SearchBar from '../components/utils/SearchBar'

/**
 * Mixin that provides basic data table functionality (such as provider function and row details)
 */
export default {
    components: {
        SearchBar
    },

    data: () => ({
        page: 1,
        perPage: 0,
        totalRows: 0,
        filter: '',
        isLoading: false
    }),

    methods: {
        fetchData: async function ({ currentPage }) {
            // Pagination is disabled for now, as it makes filtering pretty much useless
            const url = this.apiUrl.replace(encodeURIComponent('##page##'), '0')

            try {
                return await fetch(url)
                    .then(res => (this.totalRows = res.headers['X-Total-Rows']) || res)
                    .then(res => res.json())
                    // Add the row details, so it is reactive and we can toggle it with this.toggleRowDetail
                    .then(records => records.map(rec => ({ ...rec, _showDetails: false })))
            } catch (e) {
                this.showError('Unable to find records the this list.', e)
            }
        },

        toggleRowDetails: function (row) {
            row._showDetails = !row._showDetails
        }
    }
}
