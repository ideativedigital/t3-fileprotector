<?php

defined('TYPO3_MODE') or die('Access denied.');

(static function($_EXTKEY) {
    $extensionName = \TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY);

    \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
        "Ideative.$extensionName",
        'FileProtector',
        'Ideative File Protector - Request Form'
    );

    if (TYPO3_MODE === 'BE') {
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
            "Ideative.$extensionName",
            'file',
            'fileprotector',
            '',
            [
                'File' => 'backendShowDownloadRequests',
                'Backend' => 'files,requests,users,fileDetail,userDetail'
            ],
            [
                'access' => 'user,group',
                'icon' => 'EXT:id_fileprotector/Resources/Public/Icons/user_mod_fileprotector.svg',
                'labels' => 'LLL:EXT:id_fileprotector/Resources/Private/Language/locallang_db.xlf',
                'navigationComponentId' => '',
                'inheritNavigationComponentFromMainModule' => false
            ]
        );
    }

})('id_fileprotector');
