<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'File protector',
    'description' => 'Extension that allows to protect file downloads',
    'category' => '',
    'author' => 'Raphaël Gomes',
    'author_email' => 'typo3@ideative.ch',
    'state' => 'beta',
    'uploadfolder' => 0,
    'createDirs' => '',
    'clearCacheOnLoad' => 0,
    'version' => '0.1.1',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-10.0.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ],
];
